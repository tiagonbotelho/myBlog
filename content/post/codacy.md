+++
date = "2015-10-21T09:15:36+01:00"
description = "A brief story of my summer internship experience with Codacy"
title = "how being an intern at codacy helped me clean my dishes"
weight = 10
tags = ["job", "intern", "codacy"]
+++

Hello!

I should start by explaining how I got the internship, I was in the second year of my Bachelor and I was working on a project at my junior company ([jeKnowledge](http://jeknowledge.pt/)), the designer of my project based himself on the website of [Faber Ventures](http://www.faber-ventures.com/) to make the mockups. I was looking at the website and found out that there were some open positions for summer internships at some companies on which the Venture Capital Firm had invested on. 

I immediately sent my application, and after a while I received the confirmation that I was accepted to an interview. Some interviews later I eventually got accepted at Codacy. 

For those who don't know, [Codacy](https://codacy.com/) is a static code analysis company that prides itself in making your code cleaner and bug free, they do an awesome job and won the Web Summit 2014 pitch competition.

At Codacy I worked in a lot of interesting projects, some are already deployed and others will eventually get deployed. They use Scala as their primary language which I was not familiared with before the internship so it was also a challenge for me to face.

While I was working at Codacy I was also working by night on [Betar](http://www.betar.io), my own company (that will be launched by the end of the year), they were some rough months but with the support of both teams I eventually did a good job at both sides.

Jaime Jorge (CEO) and João Caxaria (CTO) will always be a real inspiration to me since they helped me enhance my coding skills, build my own company and improve my way of thinking. Also, being a person that was not very organized I was forced to schedule my whole day so that I could take the most out of it.

I finish this blogpost by thanking all members of the Codacy team for the awesome summer and the awesome opportunity they gave me.

I leave the awesome pitch that they gave at Web Summit on this [link](https://www.youtube.com/watch?v=HinOcCZdj94) as an inspiration to other people


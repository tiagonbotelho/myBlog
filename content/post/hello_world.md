+++
date = "2015-09-30T17:48:44+01:00"
description = "First Post made in this blog that introduces myself and talks a little bit about my interests"
title = "hello world"
weight = 10
tags = ["first"]
+++

Hello!

This will be the first post that I make on my blog!
I should start by introducing myself.

My name is Tiago Botelho, I'm from Portugal and I'm a student in Software Engineering at the University of Coimbra, my three biggest hobbies are my work, music (which has been with me since I was born) and sport (mainly Judo since I've practiced It for as long as I can remember).

I decided to start this blog because I sometimes feel that I should share my thoughts and experiences with the world. 

![Me](http://i.imgur.com/Vp5eTxv.jpg)

